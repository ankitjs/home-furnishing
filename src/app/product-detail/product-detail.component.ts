import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { categories } from "src/shared/static-data/data";
import * as _ from "lodash";

@Component({
  selector: "app-product-detail",
  templateUrl: "product-detail.component.html"
})
export class ProductDetailComponent implements OnInit {
  productDetail: any;
  selectedImage:any;
  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    let productId = this.route.snapshot.params.id;
    this.productDetail = _.filter(categories, { id: +productId })[0];
    this.selectedImage = this.productDetail.image_list[0];
  }
}
