import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-image-stack",
  templateUrl: "image-stack.component.html"
})
export class ImageStackComponent implements OnInit {
  @Input() imageList:any;
  @Output() public imageSelect = new EventEmitter<any>();
  constructor() {}

  ngOnInit() {
    
  }
  onImageClick(imageData:any){
    this.imageSelect.emit(imageData);
  }
}
