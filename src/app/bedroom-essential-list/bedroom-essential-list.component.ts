import { Component, OnInit, OnDestroy } from "@angular/core";
import { Observable, of, Subscription } from "rxjs";
import { categories } from "src/shared/static-data/data";
import * as _ from "lodash";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-bedroom-essential",
  templateUrl: "bedroom-essential-list.component.html"
})
export class BedRoomEssentialComponent implements OnInit, OnDestroy {
  bedroomProduct$: Observable<any>;
  filteredList: Array<any>;
  subCategory: String;
  paramsSubscription: Subscription;
  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.paramsSubscription = this.route.queryParams.subscribe(data => {
      if (data && data["sub"] && data["sub"] != "") {
        this.subCategory = data["sub"];
        this.filteredList = _.filter(categories, { sub_category: data["sub"] });
        this.bedroomProduct$ = of(_.shuffle(this.filteredList));
      } else {
        this.bedroomProduct$ = of(
          _.shuffle(_.filter(categories, { category_name: "bedroom" }))
        );
      }
    });
  }

  ngOnDestroy() {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }
}
