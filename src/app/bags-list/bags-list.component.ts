import { Component, OnInit } from "@angular/core";
import { Observable, of, Subscription } from "rxjs";
import * as _ from "lodash";
import { categories } from "src/shared/static-data/data";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-bag-list",
  templateUrl: "bags-list.component.html"
})
export class BagsListComponent implements OnInit {
  bagProduct$: Observable<any>;
  filteredList: Array<any>;
  subCategory: String;
  paramsSubscription: Subscription;
  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.paramsSubscription = this.route.queryParams.subscribe(data => {
      if (data && data["sub"] && data["sub"] != "") {
        this.subCategory = data["sub"];
        this.filteredList = _.filter(categories, { sub_category: data["sub"] });
        this.bagProduct$ = of(_.shuffle(this.filteredList));
      } else {
        this.bagProduct$ = of(
          _.shuffle(_.filter(categories, { category_name: "bags" }))
        );
      }
    });
  }

  ngOnDestroy() {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }
}
