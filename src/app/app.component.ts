import { Component, OnInit } from "@angular/core";
import { Router, NavigationEnd } from "@angular/router";
import $ from "jquery";
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html"
})
export class AppComponent implements OnInit {
  title = "home-furnishing";

  constructor(private router: Router) {}

  ngOnInit(): void {
    this.router.events.subscribe(evt => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      var body = $("html, body");
      body.animate({ scrollTop: 0 }, 300, "swing");
    });
  }
}
