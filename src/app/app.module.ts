import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgxImageZoomModule } from "ngx-image-zoom";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HeaderComponent } from "src/shared/components/header/header.component";
import { HeaderTopComponent } from "src/shared/components/header/header-top/header-top.component";
import { HeaderMiddleComponent } from "src/shared/components/header/header-middle/header-middle.component";
import { HeaderBottomComponent } from "src/shared/components/header/header-bottom/header-bottom.component";
import { SliderComponent } from "src/shared/components/slider/slider.component";
import { HomeComponent } from "src/shared/components/home/home.component";
import { LeftSidebarComponent } from "src/shared/components/home/left-sidebar/left-sidebar.component";
import { ItemContainerComponent } from "src/shared/components/home/item-container/item-container.component";
import { FeaturedItemComponent } from "src/shared/components/home/item-container/featured-items/featured-item.component";
import { CategoriesTabComponent } from "src/shared/components/home/item-container/categories-tab/categories-tab.component";
import { RecommendedComponent } from "src/shared/components/home/item-container/recommended/recommended.component";
import { FooterComponent } from "src/shared/components/footer/footer.component";
import { LandingPageComponent } from "./landing-page/landing-page.component";
import { ContactPageComponent } from "./contact-page/contact-page.component";
import { ProductCardComponent } from "src/shared/components/home/item-container/shared/product-card/product-card.component";
import { ProductActionComponent } from "src/shared/components/home/item-container/shared/product-action/product-action.component";
import { BedRoomEssentialComponent } from "./bedroom-essential-list/bedroom-essential-list.component";
import { KitchenEssentialComponent } from "./kitchen-essential-list/kitchen-essential-list.component";
import { BagsListComponent } from "./bags-list/bags-list.component";
import { TablesListComponent } from "./tables-list/tables-list.component";
import { TowelsListComponent } from "./towels-list/towels-list.component";
import { NoResultComponent } from "src/shared/components/no-result/no-result.component";
import { ProductDetailComponent } from "./product-detail/product-detail.component";
import { ZoomImageContainerComponent } from "./product-detail/shared/zoom-image-container/zoom-image-container.component";
import { ImageStackComponent } from "./product-detail/shared/image-stack/image-stack.component";
import { ProductInfoComponent } from "./product-detail/shared/product-info/product-info.component";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HeaderTopComponent,
    HeaderMiddleComponent,
    HeaderBottomComponent,
    SliderComponent,
    HomeComponent,
    LeftSidebarComponent,
    ItemContainerComponent,
    FeaturedItemComponent,
    CategoriesTabComponent,
    RecommendedComponent,
    FooterComponent,
    LandingPageComponent,
    ContactPageComponent,
    ProductCardComponent,
    ProductActionComponent,
    BedRoomEssentialComponent,
    KitchenEssentialComponent,
    BagsListComponent,
    TablesListComponent,
    TowelsListComponent,
    NoResultComponent,
    ProductDetailComponent,
    ZoomImageContainerComponent,
    ImageStackComponent,
    ProductInfoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgxImageZoomModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
