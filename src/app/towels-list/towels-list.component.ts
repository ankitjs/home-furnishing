import { Component, OnInit } from "@angular/core";
import { Observable, of } from "rxjs";
import * as _ from "lodash";
import { categories } from "src/shared/static-data/data";

@Component({
  selector: "app-towels-list",
  templateUrl: "towels-list.component.html"
})
export class TowelsListComponent implements OnInit {
  towelProduct$: Observable<any>;
  constructor() {}

  ngOnInit() {
    this.towelProduct$ = of(
      _.shuffle(_.filter(categories, { category_name: "towel" }))
    );
  }
}
