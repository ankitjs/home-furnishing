import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LandingPageComponent } from "./landing-page/landing-page.component";
import { ContactPageComponent } from "./contact-page/contact-page.component";
import { BedRoomEssentialComponent } from "./bedroom-essential-list/bedroom-essential-list.component";
import { KitchenEssentialComponent } from "./kitchen-essential-list/kitchen-essential-list.component";
import { BagsListComponent } from "./bags-list/bags-list.component";
import { TablesListComponent } from "./tables-list/tables-list.component";
import { TowelsListComponent } from "./towels-list/towels-list.component";
import { ProductDetailComponent } from "./product-detail/product-detail.component";

const routes: Routes = [
  { path: "", component: LandingPageComponent },
  { path: "contact", component: ContactPageComponent },
  { path: "bedroom_essentials", component: BedRoomEssentialComponent },
  { path: "kitchen_essentials", component: KitchenEssentialComponent },
  { path: "bags", component: BagsListComponent },
  { path: "table_linen", component: TablesListComponent },
  { path: "towels", component: TowelsListComponent },
  { path: "item_detail/:id", component: ProductDetailComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
