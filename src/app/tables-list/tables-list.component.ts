import { Component, OnInit } from "@angular/core";
import { Observable, of, Subscription } from "rxjs";
import { categories } from "src/shared/static-data/data";
import * as _ from "lodash";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-tables-list",
  templateUrl: "tables-list.component.html"
})
export class TablesListComponent implements OnInit {
  tableProduct$: Observable<any>;
  filteredList: Array<any>;
  subCategory: String;
  paramsSubscription: Subscription;
  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.paramsSubscription = this.route.queryParams.subscribe(data => {
      if (data && data["sub"] && data["sub"] != "") {
        this.subCategory = data["sub"];
        this.filteredList = _.filter(categories, { sub_category: data["sub"] });
        this.tableProduct$ = of(_.shuffle(this.filteredList));
      } else {
        this.tableProduct$ = of(
          _.shuffle(_.filter(categories, { category_name: "table_linen" }))
        );
      }
    });
  }

  ngOnDestroy() {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }
}
