import { Component, OnInit, AfterViewInit } from "@angular/core";
import $ from "jquery";
@Component({
  selector: "app-header-bottom",
  templateUrl: "header-bottom.component.html"
})
export class HeaderBottomComponent implements OnInit, AfterViewInit {
  ngAfterViewInit(): void {
    $(".dropdown-submenu a.test").on("click", function(e) {
      $(this)
        .parent()
        .parent()
        .find("ul")
        .hide();
      $(this)
        .next("ul")
        .toggle();
      e.stopPropagation();
      e.preventDefault();
    });

    $(".multidropdown-button").on("click", function(e) {
      $(".dropdown-submenu .dropdown-menu").hide();
    });
  }
  constructor() {}

  ngOnInit() {}
}
