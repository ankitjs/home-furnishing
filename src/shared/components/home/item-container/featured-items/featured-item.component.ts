import { Component, OnInit } from "@angular/core";
import { categories } from "../../../../static-data/data";
import { Observable, of } from "rxjs";
import * as _ from "lodash";
@Component({
  selector: "app-featured-item",
  templateUrl: "featured-item.component.html",
  styles:[`
  .features_items{
    min-height:0;
  }
  `]
})
export class FeaturedItemComponent implements OnInit {
  featureProductList$: Observable<any[]>;
  constructor() {}

  ngOnInit() {
    this.featureProductList$ = of(
      _.shuffle(_.filter(categories, { is_featured: true }))
    );
  }
}
