import { Component, OnInit, Input } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-product-card",
  templateUrl: "product-card.component.html"
})
export class ProductCardComponent implements OnInit {
  @Input() detail: any;
  constructor(private router: Router) {}
  ngOnInit() {}
  productDetail() {
    this.router.navigate(["item_detail", this.detail.id]);
  }
}
