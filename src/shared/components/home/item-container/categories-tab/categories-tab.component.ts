import { Component, OnInit } from "@angular/core";
import { categories } from "../../../../static-data/data";
import { Observable, of } from "rxjs";
import * as _ from "lodash";

@Component({
  selector: "app-categories-tab",
  templateUrl: "categories-tab.component.html"
})
export class CategoriesTabComponent implements OnInit {
  bedroomProduct$: Observable<any>;
  kitchenProduct$: Observable<any>;
  bagProduct$: Observable<any>;
  tableProduct$: Observable<any>;
  towelProduct$: Observable<any>;
  constructor() {}

  ngOnInit() {
    this.bedroomProduct$ = of(
      _.shuffle(_.filter(categories, { category_name: "bedroom" }))
    );
    this.kitchenProduct$ = of(
      _.shuffle(_.filter(categories, { category_name: "kitchen" }))
    );
    this.bagProduct$ = of(
      _.shuffle(_.filter(categories, { category_name: "bags" }))
    );
    this.tableProduct$ = of(
      _.shuffle(_.filter(categories, { category_name: "table_linen" }))
    );
    this.towelProduct$ = of(
      _.shuffle(_.filter(categories, { category_name: "towel" }))
    );
  }
}
