import { Component, OnInit } from "@angular/core";
import { categories } from "../../../../static-data/data";
import { Observable, of } from "rxjs";
import * as _ from "lodash";

@Component({
  selector: "app-recommended",
  templateUrl: "recommended.component.html"
})
export class RecommendedComponent implements OnInit {
  recommendedProductList$: Observable<any[]>;
  constructor() {}

  ngOnInit() {
    this.recommendedProductList$ = of(
      _.shuffle(_.filter(categories, { is_recommended: true }))
    );
  }
}
